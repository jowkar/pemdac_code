library(readxl)
library(ggplot2)

path_base <- "~/proj/pemdac_code/"
indir <- paste0(path_base,"data/")
outdir <- paste0(path_base,"/results/ctDNA/")
dir.create(outdir,showWarnings = F,recursive = T)
x <- as.data.frame(read_excel(paste0(indir,"ctDNA.xlsx")),stringsAsFactors=F)

while (dev.cur()>1) dev.off()
ggplot(x,aes(x=Day_since_first_sample,y=VAF_percent,group=Patient,
             fill=Best_overall_response,color=Best_overall_response)) + geom_line() + 
  geom_point() + theme_classic() + facet_wrap(~ Patient,scales = "free_y") + 
  ylab("Variant allele frequency (%)") + xlab("Day since first sample") + 
  theme(strip.background = element_rect(colour=NA, fill=NA), 
        panel.background = element_rect(fill = NA, color = NA)) + ylim(0,NA)
ggsave(paste0(outdir,"ctDNA_VAF.pdf"),width = 7.5*1.5,height=5)
s <- sessionInfo()
saveRDS(s,paste0(outdir,"ctDNA.sessionInfo.rda"))